(function() {
	var dStart = new Date(),
		doc = $(document),
		win = $(window),
		scrollT = win.scrollTop(),
		dataAttr,
		anchor = $('[data-id="anchor"]'),
		header = $('[data-id="header"]'),
		front = $('[data-id="front"]'),
		headerH, footerH, dEnd, totalD;

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

	var testimonialsCarouselOpts = {
		items: 1,
		loop: true,
		dots: true,
		navText: ['<span class="icon icon_angle icon_angle_reverse"></span>', '<span class="icon icon_angle"></span>'],
		animateIn: 'zoomIn',
		animateOut: 'zoomOut',
		responsive: {
			576: {
				nav: true,
			}
		}
	};
	function calcValues() {
		headerH = header.outerHeight();
	};
	function clearClasses() {
		if ( win.width() > 768 ) {
			$('[data-id="' + dataAttr + '"]').removeClass('visible');
		}
	};
	function mobileFullPageToggle() {
		$('[data-open]').click(function() {
			dataAttr = $(this).attr('data-open');
			$('[data-id="' + dataAttr + '"]').addClass('visible');
		})
		$('[data-close]').click(function() {
			$('[data-id="' + dataAttr + '"]').removeClass('visible');
		})
	};
	function calcPaddingsForWrap() {
		front.css({'padding-top': headerH});
	};
	function scrollToAnchorHref() {
		anchor.click(function(e) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top - 53
			},'slow');
		})
	};
	function addStyleToHeaderOnScroll() {
		if ( scrollT > 0 ) {
			header.addClass('header_fixed_background');
		} else if ( scrollT == 0 ) {
			header.removeClass('header_fixed_background');
		}
	}
	win.on('load resize scroll', function(e) {
		if ( e.type == 'load' ) {
			dEnd = new Date();
			totalD = dEnd - dStart;
			setTimeout( function() {
				$(".loading").fadeOut();
			}, totalD);
		}
		if ( e.type == 'resize' ) {
			waitForFinalEvent(function() {
				clearClasses();
				calcValues();
				calcPaddingsForWrap();
			}, 500, "resize")
		}
		if ( e.type == 'scroll' ) {
			scrollT = $(this).scrollTop();
			addStyleToHeaderOnScroll();
		}
	});

	doc.on('click', function(e) {
		if ( e.type == 'click' ) {
			var target = $(e.target);
		}
	});
	doc.ready(function() {
		svg4everybody();
		mobileFullPageToggle();
		addStyleToHeaderOnScroll();
		calcValues();
		calcPaddingsForWrap();
		scrollToAnchorHref();
		$('[data-carousel="testimonials"]').owlCarousel(testimonialsCarouselOpts)
	})
}())