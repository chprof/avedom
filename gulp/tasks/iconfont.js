module.exports = function () {
    $.gulp.task("iconfont", function () {
        return $.gulp.src("./app/icons/svg/*.svg")
            .pipe($.iconfontcss({
                fontName: "iconsSvg",
                cssClass: 'iconSvg',
                targetPath: '../styles/partials/iconfontSvg.scss',
                fontPath: '../fonts/'
            }))
            .pipe($.iconfont({
                prependUnicode: false,
                fontName: "iconsSvg",
                formats: ["woff", "woff2"],
                normalize: true,
                fontHeight: 1001
            }))
            .pipe($.gulp.dest("./app/fonts/"))
            .pipe($.debug({ "title": "iconfont" }));
    });
};