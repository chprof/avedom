module.exports = function () {
    $.gulp.task("cssVendor", function () {
        return $.gulp.src("./app/styles/libs/*.css")
        	.pipe($.concat('vendor.css'))
        	.pipe($.mincss({ compatibility: "ie8" }))
        	// .pipe($.gp.rename({ suffix: ".min" }))
            .pipe($.gulp.dest("./build/styles/"))
            .pipe($.debug({ "title": "cssVendor" }))
            .on("end", $.bs.reload);
    });
};